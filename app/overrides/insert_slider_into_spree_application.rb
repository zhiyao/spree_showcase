
  Deface::Override.new(
    :virtual_path => "spree/layouts/spree_application", 
    :name => "add_showcase_yield_into_spree_layout",
    :insert_before => %q{#wrapper[data-hook]},
     :text => %q{
     				<div id='spree-showcase' data-hook>
              <%= yield :spree_showcase %>
            </div>}
  )
